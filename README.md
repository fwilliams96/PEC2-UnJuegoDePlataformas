# Introducción
Bienvenidos al repositorio de la replicación realizada del videojuego de plataformas tan conocido como Super Mario Bros.

# Descripción del videojuego y cómo jugar
Dado que se trata de un videojuego de plataformas, es bastante más fácil entender las mecánicas. Como mecánicas de control usaremos las teclas A y D para desplazarse horizontalmente y la tecla espacio para saltar.
El resto de mecánicas serán las típicas de los niveles de Mario Bros, el objetivo es superar una serie de obstáculos en un tiempo límite hasta llegar al goal point que nos permitirá finalizar el nivel.
Por el camino, nos encontraremos diversos enemigos, en este caso los Goombas, a los cuales podremos eliminar saltando y cayendo encima de ellos. Esto nos proveerá de puntos que iremos sumando en nuestro HUD.
Además, existirán cajas misteriosas que podremos golpear para extraer monedas que también podremos coger.
Si el jugador choca con un enemigo sin haber caído encima o si cae al vacío, entonces el jugador automáticamente perderá la partida.
Cuando se llega al final del nivel o goal point se añadirán otros puntos más al jugador y se finalizará la partida.

# Implementación del videojuego
## Funcionalidades implementadas
Con todos los puntos básicos implementados, dentro de los puntos optativos se ha conseguido realizar todos excepto el relacionado con el powerup de Mario. Por lo que básicamente no existiría el objeto super mushroom que saldría de las cajas misteriosas ni tampoco el nuevo estado de Mario grande.
Pero por lo demás, se han implementado el resto de puntos optativos sin problema. Al final del documento se comentarán las dificultades más destacadas que se han encontrado.

## Estructura
A diferencia de la práctica anterior, se ha unificado todo en una misma escena, pero por motivos de escalabilidad si sería necesario crear otras escenas para el menú principal y otros niveles.
La organización de los scripts se ha realizado como modelo-vista-controlador, de las cuales explicaremos una a una los scripts involucrados:

### Capa modelo 
En este videojuego a diferencia de la primera práctica, la capa de modelo no tiene gran carga ya que no persistimos ni recuperamos ningún dato fuera de la partida en sí. En este caso solamente tenemos un script **Sound** que nos facilita la creación de sonidos desde el inspector del manager de sonidos que comentaremos a continuación.

### Capa lógica
En este videojuego, hay que destacar que se deben controlar los siguientes elementos:
- Lógica del videojuego en sí
- Personaje
- Enemigos
- Cajas
- Monedas
- Resto elementos que pertenecen al videojuego (tiempo límite, goal point, etc)

Empezando por la lógica central del videojuego, se concentra en el script **GameManager**, este script es bastante sencillo y básicamente se encarga de manejar los siguientes estados:
- MainMenu: Cuando apenas se abre el videojuego pero todavía no se mueve el jugador ni se está contando el tiempo.
- StartGame: Cuando el jugador ha apretado alguna tecla, transicionamos desde el estado anterior a este estado. Aquí ya podemos ver como movemos al personaje y el tiempo empieza a descontarse.
- Playing: El estado que tiene el videojuego mientras se está jugando, no tiene ninguna acción asociada (en los FSM podría contemplarse como un estado nulo o vacío).
- GameOver: Aquí se muestra la pantalla de GameOver al jugador y un botón para volver a empezar.
- Victory: Aquí en cambio mostramos la pantalla de victoria al jugador con un botón para volver a jugar también (en un videojuego con más niveles pasaríamos al siguiente nivel).
- EndGame: Sería otro estado "vacío" en el que nos encontraríamos hasta que el jugador pulsase el botón "Try again" o "Try again" de las pantallas de gameover o victory respectivamente.

Por lo que nos encontraremos que esta capa tiene gran peso respecto las otras, para comentar los scripts involucrados empezamos por los que tienen que ver con el personaje. 
Para el personaje hay que tener en cuenta tanto el movimiento como la lógica propia del personaje. El movimiento se ha separado en 3 scripts distintos
- **PlayerInput**: Primer script del movimiento del jugador que únicamente se encarga de leer las teclas del jugador y determinar si su intención es moverse horizontalmente, saltar o ambas.
- **PlayerMovement**: Es el segundo script de la cadena de movimiento que comprueba las intenciones del jugador y ejecuta la acción. Cuando éste quiere moverse horizontalmente, desplaza el rigibody con una velocidad parametrizable. Si por lo contrario desea saltar, aplica la fuerza en el eje de la Y con una fuerza de salto también parametrizable. Una vez ejecutado el movimiento notifica a las animaciones cual realizar.
- **PlayerAnimation**: Es el tercer script de la cadena de movimiento, este script es el que tiene acceso directo al componente Animator y da comienzo o para las mismas. Además se encarga de ejecutar los sonidos que sean invocados por las mismas animaciones. Es invocado tanto por el script **PlayerMovement** como el **PlayerManager**.
- **PlayerManager**: Se encarga de la lógica propia del personaje, orientado a los cambios que pueda sufrir el personaje por fuerzas externas al jugador. En este caso cuando choca con algún enemigo, cuando gana, cuando se le añaden puntos o cuando se le añaden monedas. Por dentro se comunica con el script **GameManager** para cambiar los estados de la partida.

Para manejar la lógica del enemigo, en este caso los Goombas, usamos 2 scripts distintos:
- **Waypoint Patrol**: Su objetivo es el de patrullar las zonas que nosotros le pasemos como entrada en el inspector (se tratan solo de GameObject vacíos de los cuales se obtiene el Transform), puede realizar una patrulla de más de 2 posiciones, pero en este videojuego solo hemos usado 2. Para controlar la velocidad de desplazamiento usamos el parámetro moveSpeed de la entrada.
- **EnemyManager**: Contiene la lógica propia del enemigo cuando éste interactúa con el jugador. Cuando el jugador colisiona con él, la función OnColliderEnter2D se encarga de determinar si la colisión ha sido encima o debajo de la altura Y de un parámetro de entrada llamado deathPoint. Podría haberse hecho con un collider aparte pero no se ha considerado un impacto en el videojuego. En caso de que el jugador choque por encima de esta altura el enemigo ejecuta la animación de muerte y al poco tiempo se elimina de la escena.
En caso que el jugador choque por debajo se llama al método Kill() del script **PlayerManager**, donde el jugador acabará perdiendo la partida.

Dentro de las cajas incluidas en la partida, tenemos dos scripts distintos, uno para cada una:
- **BasicBoxManager**: Se encarga únicamente de gestionar la colisión de entrada y salida contra el jugador. En ese caso empieza o termina la animación de agitar la caja. Pero dado a que la siguiente caja, la misteriosa, comparte este comportamiento, se pensó que sería interesante declarar unos métodos virtuales que luego la subclase de la caja misteriosa sobreescribiera para añadirle más funcionalidades a la caja básica.
- **MisteryBoxManager**: Como se acaba de comentar, este script hereda de **BasicBoxManager**, ya que comparte el comportamiento de agitamiento. Pero además, se ha sobreescrito los métodos declarados en la super clase para permitir sacar la moneda una vez la caja es golpeada, y de deshabilitarla. Para ello se crearon los métodos _ShowCoin()_ y _DisableBox()_ que son invocados en el método _AddExtraFunctionalityOnExit()_. 

Para la moneda, existe también un script asociado, llamado **CoinManager** pero lo único que hace es notificar al jugador de que se ha cogido una moneda (mediante colisión) y luego acabarse destruyendo.

Finalmente, tenemos otros scripts que se encargan de elementos externos:
- **DeadZone**: Script asociado a un collider por debajo de las plataformas que identifica si el jugador ha caído al vacío para notificar al **GameManager** de que ha muerto y así finalizar la partida.
- **WinZone**: Script asociado a un collider al final del nivel o goal point que notifica al **GameManager** que el jugador ha ganado la partida.
- **GameTimer**: Script asociado al temporizador que tenemos en el HUD del jugador y que nos actualiza los segundos restantes para superar el nivel, en caso de agotarse notifica al **GameManager** la derrota del jugador.
- **AudioManager**: Script asociado a la reproducción de los distintos sonidos del videojuego. A partir del **Sound** MainTheme y la lista de **Sound** llamada Effects se encarga de instanciar dos AudioSource, uno para el sonido de fondo y otro para todos los efectos de sonido restantes que podrá ser invocado mediante el mismo script con la función _Play()_ y el nombre del sonido a ejecutar. Permite modificar el volumen y el pitch asociado a cada sonido que arrastremos.

### Capa visual
Dentro de esta capa solo tenemos 3 scripts:
- **LockCameraY**: Este script es la solución a que la cámara cinemachine no siga la posición Y del jugador, así manteniendose siempre a la misma altura (la camára del videojuego original funciona así). Solamente hay que añadirlo como extensión a la virtual camera.
- **DamageEffect**: Este script se encarga de mostrar el texto correspondiente al daño realizado al Goomba cuando este es eliminado por el protagonista. Recibe el componente del texto correspondiente y un canvas group para hacer que la desaparición del mensaje sea de forma suave usando el alpha.
- **PlayerHUD**: Este script se encuentra asociado al canvas de la información de la partida y permite actualizar los 3 elementos del HUD: puntuación, monedas recogidas y tiempo restante.

## Físicas y colisiones
### Componentes de física y colisiones del jugador
![Animaciones de Mario](./Docs/ColliderAndPhysicalComponents.PNG)

En la imagen podemos ver los componentes Rigidbody2D y CapsuleCollider2D utilizados y con sus parámetros correspondientes.
Para empezar, el rigibody tiene el body type como "Dynamic" para que las fuerzas le afecten y tener una masa finita. Se le ha añadido un escala de gravedad de 8 puntos a medida que se iban haciendo las pruebas ya que se ha considerado como la más adecuada teniendo en cuenta el funcionamiento del videojuego original (las caídas son bastante rápidas).
Finalmente se le ha aplicado una pequeña fricción de 0.1 para evitar cualquier problema de desnivel en las plataformas que hiciera que el personaje con el material teflón pudiera mantenerse moviendo solo.
Como tipo de detección para las colisiones se ha usado "Continous" para evitar que por cualquier retraso en las comprobaciones el personaje pudiera traspasar alguna plataforma u otro elemento. Si algún momento pasara que el jugador traspase algún elemento antes de que el evento de colisión se active, esta opción lo detectaría y arreglaría.
El tipo de interpolación se ha usado "Interpolate" para que el movimiento se muestre de forma suavizada usando las posiciones del jugador en frames anteriores.

Para el componente CapsuleCollider2D, se ha añadido un material de tipo Teflon para evitar fricción en el personaje, esto era necesario para evitar que se quedara pegado en las plataformas o cajas al saltar contra ellas.

### Tipos de colisiones llevadas a cabo en la partida

- Colisiones mediante los eventos propios de Unity: Para la detección de colisiones con el enemigo o las monedas se usan las colisiones normales. Pero para la zona de victoria o la zona de vacío se usan triggers, de esta forma evitamos que el jugador se choque contra estas zonas realmente no visibles.
- Colisión del jugador con el suelo: A la hora de detectar si el jugador se encuentra en el suelo o no, se ha optado por usar la solución de raycasts usando el método _Physics2D.OverlapCircle_, ya que así evitamos sobreecargar los eventos de colisión del jugador y de tener que ajustar perfectamente el collider.
- Comprobación del tipo de colisión con el enemigo: Cuando el enemigo choca con el jugador (por el evento propio de Unity), dependiendo de donde hemos chocado con el enemigo, morirá o nos eliminará. Aquí se ha optado por usar una posición local del Goomba de la cual tendremos en cuenta la posición Y para determinar si el jugador ha chocado por encima de esta altura mínima (y así eliminar al Goomba) o por si lo contrario ha chocado por debajo y será el jugador el que perderá.
- Colisión con ambos tipos de cajas: Este tipo de colisión al principio parecía trivial pero finalmente con las pruebas se ha visto que realmente el jugador solo puede activarlas si choca con ellas desde abajo. Por lo que para este problema sí que se ha optado por usar dos colliders, uno para envolver la caja y otro bastante más pequeño que cubra básicamente solo la parte más baja de la caja. De esta forma solo se activa el evento cuando choca con este segundo collider. Aquí también podría haberse hecho uso de una altura máxima pero finalmente se ha optado por la solución del segundo collider ya que la forma es cuadrada a diferencia del Goomba.


## Aspectos visuales y sensoriales
Dentro de los aspectos visuales, explicaremos brevemente las diferentes animaciones que se han incluido en el videojuego:

**Animaciones del protagonista Mario**
![Animaciones de Mario](./Docs/MarioController.PNG)
Aquí podemos destacar que tiene 4 estados distintos: _Idle_, _Run_, _Jump_ y _Death_. Comentamos brevemente lo que sucede en cada estado:
- Idle: Aquí modificamos 2 propiedades distintas: el sprite y la escala. En el sprite tenemos asociado únicamente uno pero sí que se ha jugado con la escala para dar una sensación de que el personaje respira a lo largo de 40 fotogramas.
- Run: Para activarse se usa el booleano "IsRunning". Únicamente se juega con la propiedad sprite a lo largo de 10 fotogramas. Este valor se ha ajustado bastante al movimiento del videojuego original.
- Jump: Se activa con el trigger "Jump" desde cualquier estado. Dentro se usa únicamente un sprite pero a lo largo de 30 fotogramas para así forzar a la que la animación dure lo suficiente para visualizarla, ya que para volver a los estados de _Idle_ o _Run_ tiene el check de "Exit Time" marcado. Esta animación se activa mediante trigger desde cualquier estado.
- Death: Se activa con el trigger "Death" desde cualquier estado y juega únicamente con un sprite que es el que muestra a Mario con las manos en la cabeza y de frente a la pantalla.

**Animaciones del enemigo Goomba**
![Animaciones de Goomba](./Docs/GoombaController.PNG)
En este controlador encontramos únicamente dos animaciones: _Walk_ y _Death_. 
- La primera animación es la que se encuentra por defecto y es la que usa 2 sprites del Goomba para dar la sensación que camina a lo largo de 20 fotogramas.
- La otra animación de muerte, es para mostrar al Goomba chafado por Mario después de que este lo pise. Esta última se activa con el trigger "Death".

**Animaciones de la moneda**
![Animaciones de la moneda](./Docs/CoinController.PNG)
En este controlador solo tenemos una animación y es la que juega con 2 sprites distintos a lo largo de 20 fotogramas para darle un poco de vivez a la moneda.
Este controlador no usa ningún parámetro ya existe únicamente una sola animación.

**Animaciones del bloque ladrillo**
![Animaciones del bloque ladrillo](./Docs/BrickBoxController.PNG)
En este controlador podemos observar dos animaciones, la de _Quiet_ y _Shake_. La primera es el estado por defecto que no realiza ninguna animación y la segunda es la que se ejecuta cuando es golpeada por el personaje. Se activa mediante el trigger "Shake".

**Animaciones del bloque misterioso**
![Animaciones del bloque ladrillo](./Docs/MisteryBoxController.PNG)
El controlador contiene la misma estructura que el anterior controlador del ladrillo pero añade un nuevo estado, el _Disabled_ (activado mediante el trigger "Disable"). Este estado será activado cuando el jugador golpee la caja para hacer aparecer la moneda de su interior. Una vez activado este estado se mantendrá así el resto de la partida.

Dentro de los aspectos sensoriales, destacamos el uso del script **AudioManager** que permite tener centralizada la parte de los efectos de sonido y el sonido de fondo. Estos sonidos han sido tanto invocados desde código y también algunas veces invocados puntualmente desde las animaciones como "Animation Event". 

## Dificultades encontradas
Han existido varias dificultades en la realización del videojuego, ya que a pesar de parecer un videojuego sencillo implica muchas partes distintas funcionando adecuadamente y de forma coordenada. Pero de lo que podemos destacar como más difícil han sido sin dudas la realización de las animaciones, el aprendizaje de los tilemaps y unos gaps existentes entre las líneas divisorias de cada tile.
Las animaciones han sido lo más costoso, en concreto las del jugador, ya que implica saber actualizar los parámetros en el momento adecuado y con una lógica consistente en la transición de los estados. Por ejemplo, a la hora de saltar es necesario volver al estado en el que se encontraba anteriormente el jugador (parado o corriendo), por lo que era necesario comprobar si _IsRunning_ era true o false.
La dificultad de las animaciones es también la curva de aprendizaje, ya una vez se empieza a entender como funciona el tiempo de transición para fundir dos animaciones o el "Exit Time", se reduce el tiempo de realización de los controladores.

Respecto los tilemaps, vuelve a ser algo parecido a lo que sucedió con las animaciones. Al principio es costoso entender como dividir los spritesheet para posteriormente poder pintarlos en la escena usando las paletas. Hay que tener en cuenta que cada celda es 16x16 y que este ha de ser el número de pixeles por unidad para los sprites a usar, de lo contrario no ocuparán el tamaño deseado.
Una vez más se vuelve a complicar cuando se intentan añadir sprites con scripts integrados, en este caso las cajas. Ya que para ello es necesario usar el "GameObject brush" que básicamente nos permite arrastrar un prefab y poder pintarlo en la escena. Permitiéndonos así pintar las distintas cajas y las monedas en el escenario.

Respecto el error de los gaps, se vio que después de haber pintado el background y el background detail, empezaron a aparecer líneas verticales al desplazarnos por el mapa con el personaje. Se probaron distintas soluciones, pero finalmente tuvimos que quedarnos con el componente Pixel Perfect Camera que se añadió a la cámara con tal de evitar que estas líneas se vieran. Añadimos el check de Upscale Render Texture para así evitar movimientos de píxeles extraños en la animación de _Idle_. Lo que también hace que finalmente no se pueda apreciar apenas el movimiento de respiración en _Idle_.
![Gaps tiles](./Docs/GapsTiles.PNG)

Como otras dificultades encontradas, tendríamos la del movimiento del jugador, esto siempre es algo que ocupa tiempo al principio ya que se deben ir haciendo varias pruebas y así ir ajustando los valores de fuerza, velocidad y gravedad que mejor aspecto le den a la jugabilidad. Teniendo en cuenta que debe asemejarse al videojuego original al usar los mismos elementos audiovisuales.


# Presentación del videojuego
- [Demostración del videojuego](https://youtu.be/9vuPkGWrKqE)

# Publicación
- WebGL: [Un juego de plataformas para WebGL](https://informaticoloco-33.itch.io/unjuegodeplataformas)

# Licencias / Recursos utilizados
- Sonidos: [Themushroomkingdom](https://themushroomkingdom.net/) (solicitar permiso en caso de usar el contenido en un sitio web propio).
- Fuente de letra: [Fontstruct](https://fontstruct.com/) (bajo licencia de SIL Open Font License para ser usada pero sin revenderla).
- Imágenes: [Spriters Resource](https://www.spriters-resource.com/) (en este caso al ser sprites originales del videojuego no son aptos para uso comercial ).
