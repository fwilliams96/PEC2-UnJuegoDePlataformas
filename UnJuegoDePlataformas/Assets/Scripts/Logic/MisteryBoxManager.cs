using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisteryBoxManager : BasicBoxManager
{

    [SerializeField] private GameObject coin;
    
    /// <summary>
    /// Override the extra function of basic box to disable the box and show the coin inside
    /// </summary>
    protected override void AddExtraFunctionalityOnCollide()
    {
        DisableBox();
        ShowCoin();
    }

    /// <summary>
    /// The coin is already instantiated, we only active the gameobject
    /// </summary>
    private void ShowCoin()
    {
        coin.SetActive(true);
    }

    /// <summary>
    /// We disable the box by changing the bool canInteract and also triggering the disable state
    /// </summary>
    private void DisableBox()
    {
        canInteract = false;
        m_Animator.SetTrigger("Disable");
    }
}
