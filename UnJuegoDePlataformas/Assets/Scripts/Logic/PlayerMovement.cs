using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class PlayerMovement : MonoBehaviour
{
    private PlayerInput playerInput;
    private PlayerAnimation playerAnimation;
    private Rigidbody2D m_Rigidbody2D;
    private SpriteRenderer m_SpriteRenderer;

    [SerializeField] private float runSpeed = 100f;
    [SerializeField] private float jumpForce = 2f;
    [SerializeField] private float checkRadius = 0.5f;
    [SerializeField] private Transform groundCheck;

    private bool isGrounded = true;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        playerAnimation = GetComponent<PlayerAnimation>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    /// <summary>
    /// We only want to move the player while we are playing
    /// </summary>
    private void FixedUpdate()
    {
        if (GameManager.instance.gameStatus == GameStatus.Playing)
        {
            CheckGroundStatus();
            DeterminePlayerMovement();
            FlipPlayer();
        }
    }

    /// <summary>
    /// We check if we are grounded using a circle raycast and filtering by 'Platforms' layer mask
    /// </summary>
    private void CheckGroundStatus()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, 1 << LayerMask.NameToLayer("Platforms"));
    }

    /// <summary>
    /// We draw a gizmos of the circle that is checking the ground
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(groundCheck.position, checkRadius);
    }

    /// <summary>
    /// Depending on the velocity we identify if the player is moving to the right or to the left, and we flip the sprite rendered
    /// </summary>
    private void FlipPlayer()
    {
        if (playerInput.horizontal < 0)
        {
            m_SpriteRenderer.flipX = true;
        }
        else if(playerInput.horizontal > 0)
        {
            m_SpriteRenderer.flipX = false;
        }
    }

    /// <summary>
    /// We move the playr using the horizontal output variable of the PlayerInput class and we add a vertical force in case the player wants to jump.
    /// We also update the animations here using the PlayerAnimation class
    /// </summary>
    private void DeterminePlayerMovement()
    {
        // Update always the velocity to move the player and to stop him
        m_Rigidbody2D.velocity = new Vector2(playerInput.horizontal * runSpeed * Time.fixedDeltaTime, m_Rigidbody2D.velocity.y);
        
        if (playerInput.jumpPressed && isGrounded)
        {
            // Add a force in the Y axis to jump
            Debug.Log("Player jumps");
            m_Rigidbody2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            playerAnimation.PlayJumpAnimation();
        }
        else
        {
            playerAnimation.StopJumpAnimation();
        }
        
        if (playerInput.horizontal != 0 && isGrounded)
        {
            playerAnimation.PlayRunningAnimation();
        }
        else
        {
            playerAnimation.StopRunningAnimation();
        }
        
    }
}
