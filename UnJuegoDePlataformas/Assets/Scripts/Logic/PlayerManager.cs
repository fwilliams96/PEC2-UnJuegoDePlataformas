using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{

    private Rigidbody2D m_Rigibody2D;
    private Collider2D m_Collider2D;

    [SerializeField] private PlayerHUD playerHud;
    [SerializeField] private PlayerAnimation playerAnimation;

    private bool isDead = false;

    private void Awake()
    {
        m_Collider2D = GetComponent<Collider2D>();
        m_Rigibody2D = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// We play the death animation, we disable the collider to let the player drop and we finally notify the GameManager the player is dead
    /// We update the status of the isDead bool to avoid the multiple collision problem 
    /// </summary>
    public void Kill()
    {
        if (isDead) return;
        Debug.Log("Player dies");
        playerAnimation.PlayDeathAnimation();
        Invoke(nameof(DisableCollider), 1f);
        GameManager.instance.GameOver();
        isDead = true;
    }

    public void DisableCollider()
    {
        if (m_Collider2D != null)
        {
            m_Collider2D.enabled = false;    
        }
    }

    /// <summary>
    /// When the player wins we force the player to stop immediately by setting the rigidbody to kinematic and also setting all the velocities to 0
    /// Finally we notify the player hud to update the points and we also tell the GameManager the player has won
    /// </summary>
    public void Win()
    {
        Debug.Log("Player wins");
        m_Rigibody2D.isKinematic = true;
        m_Rigibody2D.velocity=new Vector2(0,0);
        m_Rigibody2D.angularVelocity = 0;
        playerHud.AddPointsOneByOne(100);
        GameManager.instance.Victory();
    }

    /// <summary>
    /// This function is called from the EnemyManager when we kill a Goomba
    /// </summary>
    /// <param name="points"></param>
    public void AddPoints(int points)
    {
        playerHud.AddPointsOneByOne(points);
    }

    /// <summary>
    /// This function is called from the CoinManager to collect a coin
    /// </summary>
    public void CollectCoin()
    {
        playerHud.CollectCoin();
    }
}
