using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimation : MonoBehaviour
{
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }
    
    public void PlayRunningAnimation()
    {
        _animator.SetBool("IsRunning", true);
    }

    public void StopRunningAnimation()
    {
        _animator.SetBool("IsRunning", false);
    }
    
    public void PlayJumpAnimation()
    {
        _animator.SetTrigger("Jump");
    }

    public void StopJumpAnimation()
    {
        _animator.ResetTrigger("Jump");
    }

    public void PlayDeathAnimation()
    {
        _animator.SetTrigger("Death");   
    }
    
    /// <summary>
    /// Function called from an animation event in the jump animation
    /// </summary>
    public void PlayJumpSound()
    {
        AudioManager.instance.Play("MarioJump");
    }
    
}