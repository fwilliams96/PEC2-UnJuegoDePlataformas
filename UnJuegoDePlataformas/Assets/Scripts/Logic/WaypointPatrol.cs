using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPatrol : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;
    [SerializeField] private float minimumDistanceToWaypoint = 0.2f;
    [SerializeField] private float moveSpeed = 4f;
    [SerializeField] private EnemyManager enemyManager;
    
    private int index = 0;
    
    /// <summary>
    /// When the enemy is still alive, we move it across all the waypoints using the MoveTowards function from Vector3.
    /// When the enemy reaches a waypoint (or almost), the index is increased to go to following one 
    /// </summary>
    void Update()
    {
        if (enemyManager.isDead) return;
        transform.position = Vector3.MoveTowards(transform.position, 
            waypoints[index].transform.position, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position, waypoints[index].transform.position) < minimumDistanceToWaypoint)
        {
            index = (index + 1) % waypoints.Count;
        }
    }
}
