using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class PlayerInput : MonoBehaviour
{
    public float horizontal;
    public bool jumpPressed;

    private bool readyToClear = false;
    
    /// <summary>
    /// We only want to read the input when the game status is Playing
    /// </summary>
    void Update()
    {
        if (GameManager.instance.gameStatus == GameStatus.Playing)
        {
            ClearInput();
            horizontal = Input.GetAxis("Horizontal");
            jumpPressed = jumpPressed || Input.GetButtonDown("Jump");
        }
    }
    
    private void FixedUpdate()
    {
        readyToClear = true;
    }
    
    /// <summary>
    /// We are ready to clean the input values in the Update when the FixedUpdate function has already been executed
    /// </summary>
    void ClearInput()
    {
        if (!readyToClear)
            return;

        horizontal		= 0f;
        jumpPressed		= false;
        
        readyToClear = false;
    }
   
}
