using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;
    
    public Sound mainTheme;

    public Sound[] effects;

    private AudioSource mainAudioSource;

    private AudioSource secondaryAudioSource;

    private void Awake()
    {
        instance = this;
        mainAudioSource = gameObject.AddComponent<AudioSource>();
        secondaryAudioSource = gameObject.AddComponent<AudioSource>();
    }

    /// <summary>
    /// We start to reproduce the main theme sound
    /// </summary>
    private void Start()
    {
        if (mainTheme != null && mainTheme.clip != null)
        {
            mainAudioSource.clip = mainTheme.clip;
            mainAudioSource.loop = true;
            mainAudioSource.Play();
        }
    }

    /// <summary>
    /// We look for the sound from the name parameter and then we reproduce it once using the secondary audio source
    /// </summary>
    /// <param name="name"></param>
    public void Play(string name)
    {
        var sound = Array.Find(effects, sound => sound.name == name);
        if (sound != null && sound.clip != null)
        {
            secondaryAudioSource.PlayOneShot(sound.clip);
        }
    }

    /// <summary>
    /// Function called from the GameManager when the game is over
    /// </summary>
    public void StopMainTheme()
    {
        mainAudioSource.Stop();
    }
}
