using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text coinText;
    [SerializeField] private Text timeText;
    
    private int scorePoints = 0;
    private int coins = 0;

    /// <summary>
    /// Coroutine to add the points one by one
    /// </summary>
    /// <param name="winPoints"></param>
    /// <returns></returns>
    IEnumerator AddWinPoints(int winPoints)
    {
        while (winPoints > 0)
        {
            AddPoints(1);
            winPoints--;
            yield return new WaitForSeconds(0.02f);
        }
    }

    /// <summary>
    /// We update the points text and we force the text to have 5 digits
    /// </summary>
    /// <param name="points"></param>
    public void AddPoints(int points)
    {
        scorePoints += points;
        scoreText.text = scorePoints.ToString("D5");
    }

    /// <summary>
    /// We add the points one by one using a coroutine to allow the player see the increment
    /// </summary>
    /// <param name="points"></param>
    public void AddPointsOneByOne(int points)
    {
        StartCoroutine(AddWinPoints(points));
    }
    
    /// <summary>
    /// We update the coins text 
    /// </summary>
    public void CollectCoin()
    {
        coins += 1;
        coinText.text = coins.ToString();
    }

    /// <summary>
    /// We update the time text formatting the time to only keep the seconds
    /// </summary>
    /// <param name="time"></param>
    public void UpdateTime(float time)
    {
        timeText.text = time.ToString("f0");
    }
}
