using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageEffect : MonoBehaviour
{
    [SerializeField] private Text damageText;
    [SerializeField] private CanvasGroup damageTextCg;
    
    /// <summary>
    /// This function is called from the EnemyManager script to show the damage text and the hide it slowly with a coroutine
    /// </summary>
    /// <param name="damagePoints"></param>
    public void DisplayDamageEffect(int damagePoints)
    {
        damageText.text = damagePoints.ToString();
        StartCoroutine(HideDamageText());
    }
    
    IEnumerator HideDamageText()
    {
        damageTextCg.alpha = 1f;
        yield return new WaitForSeconds(3.5f);
        while (damageTextCg.alpha > 0)
        {
            damageTextCg.alpha -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
